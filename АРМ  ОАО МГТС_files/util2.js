/*
 Разные полезные функциии
 
 Идеи и часть реализации sendRequest'а взяты у Peter-Paul Koch (www.quirksmode.org)

*/

function sendRequest(url, param) {
    var req = createXMLHTTPObject();

    if (!req) return;

    // Параметры
    param.method    = param.method      || "GET";
    param.postData  = param.data        || "";
    param.callback  = param.callback    || null;
    param.onError   = param.onError     || null;

    req.open(param.method, url, true);
    req.setRequestHeader('User-Agent','XMLHTTP/1.0');
    if (param.method == "POST") {
        req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    }

    req.onreadystatechange = function () {
        if (req.readyState != 4) return;
        if (req.status >= 400) {
            if (param.onError) {
                param.onError(req);
            }
            return;
        }
        param.callback(req);
    }
    if (req.readyState == 4) {
        return;
    }

    req.send(param.postData);
}

var XMLHttpFactories = [
    function () {return new XMLHttpRequest()},
    function () {return new ActiveXObject("Msxml2.XMLHTTP")},
    function () {return new ActiveXObject("Msxml3.XMLHTTP")},
    function () {return new ActiveXObject("Microsoft.XMLHTTP")}
];

function createXMLHTTPObject() {
    var xmlhttp = false;
    for (var i=0;i<XMLHttpFactories.length;i++) {
        try {
            xmlhttp = XMLHttpFactories[i]();
        } catch (e) {
            continue;
        }
        break;
    }
    return xmlhttp;
}

// Стырил пару функций у http://www.dustindiaz.com/top-ten-javascript/
function addEvent(elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
		elm.addEventListener(evType, fn, useCapture);
	} else if (elm.attachEvent) {
		elm.attachEvent('on' + evType, fn);
	}
}

// Мегафункция для ленивых программистов
function $$() {
	var elements = new Array();

	for (var i = 0; i < arguments.length; i++) {
		var element = arguments[i];
		if (typeof element == 'string')
			element = document.getElementById(element);
		if (arguments.length == 1)
			return element;
		elements.push(element);
	}
	return elements;
}

function $text(element, text) {
    if (element && element.appendChild) {
        if (text && typeof text == "string") {
            element.appendChild(document.createTextNode(text));
        } else {
            element.appendChild(document.createTextNode(text));
        }
    }
}

/* -- EOF -- */

