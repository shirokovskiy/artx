var searchURI   = 'AddrInfo.phtml?format=json&prefix=';
var streetURI   = 'AddrInfo.phtml?format=json&street=';

var strokeTimeout = 500;

function removeChildren(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

function appendLi(list, text) {
    var li = document.createElement('li');
    list.appendChild(li);

    li.style.lineHeight = '1.7em';

    if (text && typeof text == 'string') {
        $text(list, text);
    }
    return li;
}

function appendLink(list, params, text) {
    var li   = appendLi(list);
    var link = document.createElement('span');

    if (params.onclick) {
        addEvent(link, 'click', params.onclick);
    }

    link.title = params.title || text || '';

    link.style.borderBottom = '1px dashed blue';
    link.style.cursor       = 'pointer';

    text = text || params.title || '[]';
    $text(link, text);
    li.appendChild(link);

    return link;
}

function AddrSearch(element, params, prf) {
    this.element = $$(element);
    var params = params || {};

    if (!this.element) {
        throw 'Не задан элемент';
    }

    if (typeof(this.element.value) == 'undefined') {
        throw 'Не получить аттрибут value';
    }
    if (!prf) prf = '';

    this.timerId         = null;
    this.streets         = $$(params.streets)     || $$('streets'+prf);
    this.buildings       = $$(params.buildings)   || $$('buildings'+prf);
    this.streetHandler   = params.streetHandler   || this.defaultStreetHandler;
    this.buildingHandler = params.buildingHandler || this.defaultBuildingHandler;
    this.houseNum = '';
    this.houseStatus     = params.houseStatus   || '0';

    this.streets.style.listStyleType = this.streets.style.listStyleType = 'none';
    this.streets.style.padding       = this.streets.style.padding       = 0;
    this.streets.style.margin        = this.streets.style.margin        = '1em';


    function makeSearch(pointer) {
        return function() {
            pointer.search();
        }
    }
    addEvent(this.element, 'keyup', makeSearch(this)); 
    
}

AddrSearch.prototype.search = function() {
    var prefix = this.element.value;

    if (prefix.length < 3) {
        return true;
    }

    var query = encodeURI(searchURI + prefix);
    //alert(query);
    if (this.timerId) {
        clearTimeout(this.timerId);
    }

    function makeSearch(pointer) {
        return function() {
            sendRequest(query, { callback: function(req) { pointer.searchResult(req);} });
        }
    }

    this.timerId = setTimeout(makeSearch(this), strokeTimeout);

    return true;
}

  AddrSearch.prototype.searchResult = function(req,prf) {
    this.streets.style.display = 'block';

    if (!prf) prf = '';

    var houseNumObj   = $$('houseNum'+prf);
    if (houseNumObj)  this.houseNum   = houseNumObj.value;

    var res = eval('(' + req.responseText + ')');

    removeChildren(this.streets);

    var len = res.length;
    for (var i = 0; i < len; i++) {
        appendLink(this.streets, {
	  onclick: this.streetHandler(res[i].id, res[i].name,res[i].qty, this.houseNum) }, res[i].name);
    }

    if (len > 1) {
        removeChildren(this.buildings);
    }
    
    // При получении только одной улицы сразу выводить дома (via alexd)
    if (len == 1) {
        if (this.buildings && this.buildings.current != res[0].id) {
            removeChildren(this.buildings);
        }
        (this.streetHandler(res[0].id, res[0].name, res[0].qty, this.houseNum))();
    }

    if (len == 0) {
        removeChildren(this.buildings);
        appendLi(this.streets, 'Не найдено ни одной улицы');
        if ($$("house_id"+prf)) $$("house_id"+prf).value    = 0;
	if ($$("prefix"+prf)) $$("prefix"+prf).value = '';
	if ($$("qty_chan"+prf)) $$("qty_chan"+prf).value = '';
    }
}

    AddrSearch.prototype.defaultStreetHandler = function(id, name, qty, houseNum) {
    pointer = this;
    return function() {
        var infoURI = streetURI + id;
	if (houseNum) infoURI = infoURI + '&houseNum='+houseNum;
	if (pointer.houseStatus) infoURI = infoURI + '&houseStatus='+pointer.houseStatus;

        pointer.buildings.current = id;
        pointer.buildings.currentName = name;
        pointer.buildings.currentQtyChan = qty;
        sendRequest(infoURI, {callback: function(req) { pointer.showStreetInfo(req);}});
        return false;
    }
}

AddrSearch.prototype.showStreetInfo = function(req) {
    this.buildings.style.display = 'block';

    var res = eval('(' + req.responseText + ')');

    removeChildren(this.buildings);

    // IE bug fighting
    if (navigator.appName == 'Microsoft Internet Explorer'
     && /6.0/.test(navigator.userAgent)
     && this.buildings.currentStyle.position == 'absolute') {

        if (! this.iframe) {
            this.iframe = document.createElement('<iframe frameborder="0" src="about:blank">');

            this.iframe.style.border = 'none';
            this.iframe.style.position = 'absolute';
            this.iframe.style.zIndex = -1;
            this.iframe.style.filter = 'alpha(opacity = 0)';

        }
        var bList = this.buildings;
        var iframe = this.iframe;


        var len = res.length;
        for (var i = 0; i < len; i++) {
            var link = appendLink(this.buildings, {
                onclick: this.buildingHandler(res[i].id, res[i].num, res[i].qty_chan)
                }, res[i].num);

            link.attachEvent('onclick', function() {
                iframe.style.display = 'none';
            });
        }

        iframe.style.width  = bList.offsetWidth;
        iframe.style.height = bList.offsetHeight;
        iframe.style.left   = bList.offsetLeft;

        iframe.style.zIndex = 100;
        bList.style.zIndex  = 101;
        
        window.attachEvent('onscroll', function () {
            iframe.parentNode.removeChild(iframe);
            bList.parentNode.insertBefore(iframe, this.buildings);
        });

        bList.parentNode.insertBefore(iframe, this.buildings);
        iframe.style.display = 'block';
        
        //iframe.style.display = this.buildings.style.display;
    } else {
        var len = res.length;
        for (var i = 0; i < len; i++) {
            appendLink(this.buildings, {
                onclick: this.buildingHandler(res[i].id, res[i].num, res[i].qty_chan)
                }, res[i].num);
        }
    }
}

  AddrSearch.prototype.defaultBuildingHandler = function(id, name, qty_chan) {
    pointer = this;
    return function() {
        // pointer.bldContainer.style.display = 'none';
        pointer.buildings.style.display = 'none';

        return false;
    }
}

/* -- EOF -- */

